#!/usr/bin/env bash

# I couldn't figure out how to do this one by solely
# delegating to POSIX utilities.

# The brain-dead self-explanatory comments are for me when I
# look at this later.

# Doubles its argument; if the resulting value is two digits,
# subtracts 9.
quasidouble() {
  local -i i=$1
  if [[ $i -lt 5 ]] ; then
    echo $(( 2 * i ))
  else
    echo $(( i * 2 - 9 ))
  fi
}

# Having this as a function makes for cleaner early returns.
return_false() {
  echo "false"
  exit 0
}

# The £ symbol makes rev choke and die, so we have to check
# for invalid characters up front.
if echo "$1" | grep -Eqv '^[[:space:][:digit:]]+$'; then return_false; fi

# Strip spaces, reverse order, and read in the output as an
# array of individual digits.
readarray -t digits < <(echo "$1" | tr -d [[:space:]] | rev | grep -o . )

# Single (or zero) -digit strings are invalid.
if [[ ${#digits[@]} -lt 2 ]]; then return_false; fi

declare -i accum=0
for i in "${!digits[@]}"; do
  d=${digits[i]}

  if [[ $(( i % 2 )) == 0 ]]; then
     (( accum = accum + d))
  else
    n=$(quasidouble "$d")
    (( accum = accum + n))
  fi
done

if [[ $(( accum % 10 )) -ne 0 ]]; then return_false; fi
echo "true"

