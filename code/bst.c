#include "binary_search_tree.h"

#include <stdlib.h>
#include <stdio.h>

// Scream and die on failure to allocate.
static int die() {
  perror("unable to allocate memory");
  exit(1);
}

// Return a new leaf node with the given data value.
static node_t *new_node(int val) {
  node_t *leaf = malloc(sizeof(node_t));
  if(leaf == NULL) { die(); }

  leaf->left = NULL;
  leaf->right = NULL;
  leaf->data = val;

  return leaf;
}

// Insert a new node into the tree with the given value.
static void insert_node(node_t *tree, int val) {
  if(val <= tree->data) {
    if(tree->left == NULL) {
      tree->left = new_node(val);
    } else {
      insert_node(tree->left, val);
    }
  } else {
    if(tree->right == NULL) {
      tree->right = new_node(val);
    } else {
      insert_node(tree->right, val);
    }
  }
}

// Return the total number of elements in the tree.
static size_t length_tree(node_t *tree) {
  if(tree == NULL) {
    return 0;
  } else {
    return 1 + length_tree(tree->left)
             + length_tree(tree->right);
  }
}

void free_tree(node_t *tree) {
  if(tree->left != NULL) {
    free_tree(tree->left);
  }
  if(tree->right != NULL) {
    free_tree(tree->right);
  }
  free(tree);
}

node_t *build_tree(int *tree_data, size_t tree_data_len) {
  if(tree_data_len == 0) {
    return NULL;
  }

  node_t *tree = new_node(tree_data[0]);
  for(size_t i = 1; i < tree_data_len; ++i) {
    insert_node(tree, tree_data[i]);
  }

  return tree;
}

// Recursive tree-walker to fill a pre-allocated buffer with
// the tree's data (in order).
//
// Call initially thus:
// size_t idx = 0;
// sort_walker(tree_root, buffer, &idx);
static void sort_walker(node_t *tree, int *data, size_t *idx) {
  if(tree == NULL) {
    return;
  }
  
  if(tree->left != NULL) {
    sort_walker(tree->left, data, idx);
  }

  data[(*idx)++] = tree->data;

  if(tree->right != NULL) {
    sort_walker(tree->right, data, idx);
  }
}

int *sorted_data(node_t *tree) {
  size_t length = length_tree(tree);
  int* data = (int*) malloc(length * sizeof(int));
  if(data == NULL) { die(); }

  size_t idx = 0;
  
  sort_walker(tree, data, &idx);

  return data;
}




#ifndef BINARY_SEARCH_TREE_H
#define BINARY_SEARCH_TREE_H
#include <stddef.h>

typedef struct node node_t;

struct node {
   node_t *right;
   node_t *left;
   int data;
};

node_t *build_tree(int *tree_data, size_t tree_data_len);
void free_tree(node_t *tree);
int *sorted_data(node_t *tree);

#endif

