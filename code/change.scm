(import (rnrs))

;; Return the smallest coin set that makes up amt-left in change pushed
;; on to the alread-used coins accum, or else #f if it's impossible to
;; make amt-left with the given coins.
;;
;; This function is recursive, and should be called initially thus:
;; (change-aux #f '() amount 0 coin-denominations)
(define (change-aux shortest accum amt-left coin-n coins)
    (cond
        ((= 0 amt-left) accum)
        ((and shortest (<= shortest (length accum))) #f)
        (else
            (let*
                    ((this-coin-list
                        (if (< coin-n (length coins))
                            (if (<= (list-ref coins coin-n) amt-left)
                                (change-aux shortest
                                            (cons (list-ref coins coin-n) accum)
                                            (- amt-left (list-ref coins coin-n))
                                            coin-n coins)
                                #f)
                            #f))
                    (cur-shortest (if this-coin-list
                                          (length this-coin-list)
                                          shortest))
                    (next-coin-list
                        (if (<= (length coins) coin-n)
                            #f
                            (change-aux cur-shortest accum amt-left
                                        (+ 1 coin-n) coins))))
            
                (cond
                    ((and this-coin-list next-coin-list)
                        (if (< (length this-coin-list) (length next-coin-list))
                            this-coin-list
                            next-coin-list))
                    ((or this-coin-list next-coin-list)
                        (or this-coin-list next-coin-list))
                    (else #f))))))

(define (change amount coins)
    (let ((shortest (change-aux #f '() amount 0 (sort coins >))))
        (if shortest
            shortest
            (throw 'misc-error "invalid input combination"))))


