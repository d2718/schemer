short :: TokenType -> String

short CommentTok        = "co"
short DocumentationTok  = "do"
short AnnotationTok     = "an"
short CommentVarTok     = "cv"

short ImportTok         = "im"

short DataTypeTok       = "dt"

short ConstantTok       = "cn"
short BuiltInTok        = "bu"
short DecValTok         = "dv"
short BaseNTok          = "bn"
short FloatTok          = "fl"
short CharTok           = "ch"
short SpecialCharTok    = "sc"

short StringTok         = "st"
short VerbatimStringTok = "vs"
short SpecialStringTok  = "ss"

short KeywordTok        = "kw"
short ControlFlowTok    = "cf"
short OperatorTok       = "op"

short VariableTok       = "va"

short AttributeTok      = "at"

short FunctionTok       = "fu"

short PreprocessorTok   = "pp"

short RegionMarkerTok   = "re"

short ExtensionTok      = "ex"

short InformationTok    = "in"
short AlertTok          = "al"
short ErrorTok          = "er"
short WarningTok        = "wa"

short NormalTok         = ""

short OtherTok          = "ot"

