#!/bin/sh

elm make  --optimize --output schemer.js src/Main.elm && \
  npx swc schemer.js -o schemer.min.js
