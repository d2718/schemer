#!/bin/bash

TGT=dan@d2718.net:~/wr/schemer

scp schemer.html "$TGT/index.html"
scp schemer.min.js "$TGT"
scp schemer.css "$TGT"

scp code/*.html "$TGT/code/"
