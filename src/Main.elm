port module Main exposing (main)

import Browser
import Color exposing (Color)
import File exposing (File)
import File.Download as Download
import File.Select exposing (file)
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Http
import Json.Decode as Decode
import Json.Encode as Encode
import Lang exposing (Lang)
import Save
import Stylesheet
import Task
import Theme exposing (Theme)
import Util


main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        , view = view
        }



-- Ports


port alert : String -> Cmd msg


type alias Model =
    { theme : Theme
    , bg : Color
    , lang : Lang
    }


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model Theme.defaultTheme (Color.rgb 48 48 48) Lang.Rust
    , Cmd.none
    )



-- Messages


type Msg
    = Update Theme
    | Background Color
    | SaveTheme String
    | SelectFile
    | Load File
    | DecodeLoaded String
    | SelectLang Lang
    | CodeReturned (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Update t ->
            ( { model | theme = t }
            , Stylesheet.toStyler (Stylesheet.sheetFromTheme t)
            )

        Background c ->
            ( { model | bg = c }, Cmd.none )

        SaveTheme s ->
            ( model
            , Download.string "scheme.json" "application/json" s
            )

        SelectFile ->
            ( model, file [ "application/json" ] Load )

        Load f ->
            ( model, Task.perform DecodeLoaded (File.toString f) )

        DecodeLoaded json ->
            case Decode.decodeString Save.themeDecoder json of
                Ok t ->
                    ( { model | theme = t }
                    , Stylesheet.toStyler (Stylesheet.sheetFromTheme t)
                    )

                Err e ->
                    ( model, Decode.errorToString e |> alert )

        SelectLang lang ->
            ( { model | lang = lang }, Lang.getLangCode CodeReturned lang )

        CodeReturned (Err e) ->
            ( model, Util.httpErrMsg e |> alert )

        CodeReturned (Ok text) ->
            ( model, Lang.toCode text )



-- View


view : Model -> Html Msg
view model =
    Html.div
        [ Attributes.style "background-color" (Color.cssStr model.bg)
        , Attributes.style "color" (Color.contrast model.bg |> Color.cssStr)
        ]
        [ Html.header []
            [ Html.input
                [ Attributes.type_ "range"
                , Attributes.value (String.fromInt model.bg.r)
                , Attributes.max "255"
                , Attributes.min "0"
                , Events.onInput
                    (String.toInt
                        >> Maybe.withDefault 0
                        >> Color.grey
                        >> Background
                    )
                ]
                []
            , Html.button
                [ Events.onClick
                    (Save.encodeTheme
                        model.theme
                        |> Encode.encode 0
                        |> SaveTheme
                    )
                ]
                [ Html.text "save" ]
            , Html.button
                [ Events.onClick SelectFile ]
                [ Html.text "load" ]
            , Lang.langSelect SelectLang model.lang
            ]
        , Theme.view Update model.theme
        ]
