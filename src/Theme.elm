module Theme exposing (..)

import Color exposing (Color)
import Dict exposing (Dict)
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Pal exposing (Entry, Palette)
import Span exposing (Span(..))
import Style exposing (Style)
import Util


type alias Theme =
    { name : String
    , pal : Palette
    , default : Style
    , styles : Dict Int Style
    }


defaultTheme : Theme
defaultTheme =
    Theme
        "A Theme"
        Pal.minimal
        (Style Span.Default (Just 1) (Just 0) Nothing Nothing Nothing)
        Dict.empty


updDefault : Theme -> Style -> Theme
updDefault t style =
    { t | default = style }


updPalette : Theme -> Palette -> Theme
updPalette t pal =
    { t | pal = pal }


updStyle : Theme -> Style -> Theme
updStyle t style =
    case style.span of
        Default ->
            updDefault t style

        _ ->
            { t | styles = Dict.insert (Span.asInt style.span) style t.styles }


getStyle : Theme -> Span -> Maybe Style
getStyle t span =
    case span of
        Default ->
            Just t.default

        _ ->
            Dict.get (Span.asInt span) t.styles


getFacet : Theme -> (Style -> Maybe a) -> Span -> Maybe a
getFacet t extract span =
    case getStyle t span |> Maybe.andThen extract of
        Just x ->
            Just x

        Nothing ->
            Span.fallback span |> Maybe.andThen (getFacet t extract)


getFg : Theme -> Span -> Maybe Color
getFg t span =
    getFacet t (Style.getFgCol t.pal) span


getBg : Theme -> Span -> Maybe Color
getBg t span =
    getFacet t (Style.getBgCol t.pal) span


getBold : Theme -> Span -> Maybe Bool
getBold t span =
    getFacet t Style.getBold span


getItalic : Theme -> Span -> Maybe Bool
getItalic t span =
    getFacet t Style.getItalic span


getUline : Theme -> Span -> Maybe Bool
getUline t span =
    getFacet t Style.getUline span



-- Theme View


colorOption : Entry -> Html a
colorOption ent =
    Html.option
        [ String.fromInt ent.id |> Attributes.value ]
        [ Pal.getText ent.name |> Html.text ]


colorRule : String -> Maybe Color -> Maybe (Html.Attribute a)
colorRule property maybeColor =
    maybeColor
        |> Maybe.andThen (Color.cssStr >> Attributes.style property >> Just)


boolRule : String -> String -> String -> Maybe Bool -> Maybe (Html.Attribute a)
boolRule property trueVal falseVal maybeb =
    case maybeb of
        Nothing ->
            Nothing

        Just True ->
            Just (Attributes.style property trueVal)

        Just False ->
            Just (Attributes.style property falseVal)


cssList : Theme -> Span -> List (Html.Attribute a)
cssList t span =
    [ getFg t span |> colorRule "color"
    , getBg t span |> colorRule "background-color"
    , getBold t span |> boolRule "font-weight" "bold" "normal"
    , getItalic t span |> boolRule "font-style" "italic" "normal"
    , getUline t span |> boolRule "text-decoration-line" "underline" "none"
    ]
        |> Util.keepJust


styleRow : (Theme -> a) -> Theme -> List (Html a) -> Style -> Html a
styleRow update t options style =
    Style.view
        (updStyle t >> update)
        (cssList t style.span)
        options
        style


styleList : (Theme -> a) -> Theme -> Html a
styleList update t =
    let
        options =
            Dict.values t.pal.entries
                |> List.map colorOption
                |> (::)
                    (Html.option [ Attributes.value Style.noneColorVal ]
                        [ Html.text "[none]" ]
                    )
    in
    Html.table
        (Attributes.id "theme-table" :: cssList t Span.Default)
        [ Html.thead []
            [ Html.th [] [ Html.text "style" ]
            , Html.th [] [ Html.text "fg" ]
            , Html.th [] [ Html.text "bg" ]
            , Html.th [] [ Html.text "bold/italic/uline" ]
            ]
        , Html.tbody []
            (Span.all
                |> List.map
                    (\span ->
                        getStyle t span
                            |> Maybe.withDefault (Style.new span)
                    )
                |> List.map (styleRow update t options)
            )
        ]


view : (Theme -> a) -> Theme -> Html a
view update t =
    Html.div [ Attributes.id "theme" ]
        [ Pal.view (updPalette t >> update) t.pal
        , Html.div [] [ styleList update t ]
        ]
