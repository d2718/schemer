module Color exposing (..)

import Util exposing (byte)


type Elt
    = R
    | G
    | B


type alias Color =
    { r : Int
    , g : Int
    , b : Int
    }


black : Color
black =
    Color 0 0 0


dark : Color
dark =
    Color 64 64 64


light : Color
light =
    Color 192 192 192


white : Color
white =
    Color 255 255 255


grey : Int -> Color
grey n =
    let
        v =
            byte n
    in
    Color v v v


rgb : Int -> Int -> Int -> Color
rgb r g b =
    Color (byte r) (byte g) (byte b)


copy : Color -> Color
copy { r, g, b } =
    rgb r g b


contrast : Color -> Color
contrast { r, g, b } =
    if r + g + b < 390 then
        light

    else
        dark


elt : Elt -> Color -> Int
elt e c =
    case e of
        R ->
            c.r

        G ->
            c.g

        B ->
            c.b


update : Elt -> Color -> Int -> Color
update e c val =
    case e of
        R ->
            { c | r = byte val }

        G ->
            { c | g = byte val }

        B ->
            { c | b = byte val }


hexStr : Color -> String
hexStr { r, g, b } =
    [ r, g, b ]
        |> List.map Util.byteToHex
        |> String.join ""


cssStr : Color -> String
cssStr c =
    String.cons '#' (hexStr c)


fromHex : String -> Color
fromHex s =
    case
        String.trim s
            |> String.toList
            |> List.filter Char.isHexDigit
            |> List.map Util.fromHexDigit
    of
        [ rh, rl, gh, gl, bh, bl ] ->
            Color (rh * 16 + rl) (gh * 16 + gl) (bh * 16 + bl)

        _ ->
            black


fromHexElts : String -> String -> String -> Color
fromHexElts r g b =
    Color (Util.hexToByteOr 0 r)
        (Util.hexToByteOr 0 g)
        (Util.hexToByteOr 0 b)
