port module Lang exposing (Lang(..), getLangCode, langSelect, toCode)

import Html exposing (Html)
import Html.Attributes as Attributes
import Http
import Util


port toCode : String -> Cmd msg


type Lang
    = Rust
    | Bash
    | C
    | Elixir
    | Elm
    | Go
    | Lua
    | Scheme


all : List Lang
all =
    [ Rust, Bash, C, Elixir, Elm, Go, Lua, Scheme ]


asString : Lang -> String
asString lang =
    case lang of
        Rust ->
            "Rust"

        Bash ->
            "Bash"

        C ->
            "C"

        Elixir ->
            "Elixir"

        Elm ->
            "Elm"

        Go ->
            "Go"

        Lua ->
            "Lua"

        Scheme ->
            "Scheme"


fromString : String -> Lang
fromString str =
    case str of
        "Bash" ->
            Bash

        "C" ->
            C

        "Elixir" ->
            Elixir

        "Elm" ->
            Elm

        "Go" ->
            Go

        "Lua" ->
            Lua

        "Scheme" ->
            Scheme

        _ ->
            Rust


toUrl : Lang -> String
toUrl lang =
    case lang of
        Rust ->
            "code/rust.html"

        Bash ->
            "code/bash.html"

        C ->
            "code/c.html"

        Elixir ->
            "code/elixir.html"

        Elm ->
            "code/elm.html"

        Go ->
            "code/go.html"

        Lua ->
            "code/lua.html"

        Scheme ->
            "code/scheme.html"


getLangCode : (Result Http.Error String -> msg) -> Lang -> Cmd msg
getLangCode cback lang =
    Http.get
        { url = toUrl lang
        , expect = Http.expectString cback
        }


asOption : Lang -> Html a
asOption lang =
    Html.option
        [ Attributes.value (asString lang) ]
        [ Html.text (asString lang) ]


langSelect : (Lang -> msg) -> Lang -> Html msg
langSelect cback selectedLang =
    Html.select
        [ Util.onInput (fromString >> cback)
        , Attributes.value (asString selectedLang)
        ]
        (List.map asOption all)
