module Util exposing (..)

import Hex
import Html
import Html.Attributes as Attributes
import Html.Events as Events
import Http
import Json.Decode as Decode exposing (Decoder)


isJust : Maybe a -> Bool
isJust maybe =
    case maybe of
        Nothing ->
            False

        Just _ ->
            True


isNothing : Maybe a -> Bool
isNothing maybe =
    case maybe of
        Nothing ->
            True

        Just _ ->
            False


keepJust : List (Maybe a) -> List a
keepJust list =
    List.filterMap (\x -> x) list


tryFromHexDigit : Char -> Maybe Int
tryFromHexDigit c =
    case Char.toLower c of
        '0' ->
            Just 0

        '1' ->
            Just 1

        '2' ->
            Just 2

        '3' ->
            Just 3

        '4' ->
            Just 4

        '5' ->
            Just 5

        '6' ->
            Just 6

        '7' ->
            Just 7

        '8' ->
            Just 8

        '9' ->
            Just 9

        'a' ->
            Just 10

        'b' ->
            Just 11

        'c' ->
            Just 12

        'd' ->
            Just 13

        'e' ->
            Just 14

        'f' ->
            Just 15

        _ ->
            Nothing


fromHexDigit : Char -> Int
fromHexDigit c =
    tryFromHexDigit c |> Maybe.withDefault 0


byte : Int -> Int
byte n =
    modBy 256 n


byteToHex : Int -> String
byteToHex n =
    let
        b =
            byte n
    in
    if b < 16 then
        "0" ++ Hex.toString b

    else
        Hex.toString b


hexToByte : String -> Result String Int
hexToByte s =
    case Hex.fromString s of
        Ok n ->
            Ok (byte n)

        Err _ ->
            Err ("invalid hex: " ++ s)


hexToByteOr : Int -> String -> Int
hexToByteOr default s =
    (Hex.fromString s |> Result.withDefault default) |> byte


onEvent : String -> (String -> a) -> Html.Attribute a
onEvent event update =
    let
        decoder : Decoder a
        decoder =
            Decode.map update
                (Decode.at [ "target", "value" ] Decode.string)
    in
    Events.on event decoder


onInput : (String -> a) -> Html.Attribute a
onInput update =
    onEvent "input" update


onChange : (String -> a) -> Html.Attribute a
onChange update =
    onEvent "change" update


threeWayValToBool : String -> Maybe Bool
threeWayValToBool val =
    case val of
        "yes" ->
            Just True

        "no" ->
            Just False

        _ ->
            Nothing


boolToThreeWayVal : Maybe Bool -> String
boolToThreeWayVal b =
    case b of
        Just True ->
            "yes"

        Just False ->
            "no"

        nothing ->
            "???"


threeWay : (Maybe Bool -> a) -> String -> Maybe Bool -> Html.Html a
threeWay update title b =
    Html.select
        [ boolToThreeWayVal b |> Attributes.value
        , onInput (threeWayValToBool >> update)
        , Attributes.title title
        ]
        [ Html.option [ Attributes.value "???" ] [ Html.text " - " ]
        , Html.option [ Attributes.value "yes" ] [ Html.text "✓" ]
        , Html.option [ Attributes.value "no" ] [ Html.text "✗" ]
        ]

httpErrMsg : Http.Error -> String
httpErrMsg err =
    case err of
        Http.BadUrl s -> "bad URL: " ++ s
        Http.Timeout -> "network timeout"
        Http.NetworkError -> "unspecified network error"
        Http.BadStatus n -> "request returned failure status: " ++ (String.fromInt n)
        Http.BadBody s -> "error with response body: " ++ s
