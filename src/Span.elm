module Span exposing
    ( Span(..)
    , all
    , asInt
    , fallback
    , fromInt
    , name
    , nick
    )


type Span
    = Default
    | Keyword -- k
    | KwdDecl -- kd
    | KwdNS -- kn
    | PseudoKwd -- kp
    | KwdReserved -- kr
    | KwdType -- kt
    | Name
    | NameAttr -- na
    | NameBltn -- nb
    | NameTag -- nt
    | Const
    | ConstBltn -- kc
    | ConstDate -- ld
    | ConstNum -- m
    | ConstNumFloat -- mf
    | ConstNumInt -- mi
    | ConstNumLong -- il
    | ConstNumBin -- mb
    | ConstNumOct -- .mo
    | ConstNumHex -- mh
    | Str -- s
    | Str1 -- s1
    | Str2 -- s2
    | StrBacktick -- sb
    | StrDelim -- dl
    | StrHereDoc -- sh
    | StrAffix -- sa
    | StrIntrp -- si
    | Chr -- sc
    | ChrEsc -- se
    | Comment -- c
    | Comment1 -- c1
    | CommentMul -- cm
    | CommentDoc -- sd
    | CommentSpc -- cs
    | Shebang -- ch
    | Pproc -- cp
    | PprocFile -- cpf


asInt : Span -> Int
asInt span =
    case span of
        Default ->
            0

        Keyword ->
            1

        KwdDecl ->
            2

        KwdNS ->
            3

        PseudoKwd ->
            4

        KwdReserved ->
            5

        KwdType ->
            6

        Name ->
            7

        NameAttr ->
            8

        NameBltn ->
            9

        NameTag ->
            10

        Const ->
            11

        ConstBltn ->
            12

        ConstDate ->
            13

        ConstNum ->
            14

        ConstNumFloat ->
            15

        ConstNumInt ->
            16

        ConstNumLong ->
            17

        ConstNumBin ->
            18

        ConstNumOct ->
            19

        ConstNumHex ->
            20

        Str ->
            21

        StrDelim ->
            22

        Str1 ->
            23

        Str2 ->
            24

        StrBacktick ->
            25

        StrHereDoc ->
            26

        StrAffix ->
            27

        StrIntrp ->
            28

        Chr ->
            29

        ChrEsc ->
            30

        Comment ->
            31

        Comment1 ->
            32

        CommentMul ->
            33

        CommentDoc ->
            34

        CommentSpc ->
            35

        Shebang ->
            36

        Pproc ->
            37

        PprocFile ->
            38


fromInt : Int -> Span
fromInt n =
    case n of
        1 ->
            Keyword

        2 ->
            KwdDecl

        3 ->
            KwdNS

        4 ->
            PseudoKwd

        5 ->
            KwdReserved

        6 ->
            KwdType

        7 ->
            Name

        8 ->
            NameAttr

        9 ->
            NameBltn

        10 ->
            NameTag

        11 ->
            Const

        12 ->
            ConstDate

        13 ->
            ConstBltn

        14 ->
            ConstNum

        15 ->
            ConstNumFloat

        16 ->
            ConstNumInt

        17 ->
            ConstNumLong

        18 ->
            ConstNumBin

        19 ->
            ConstNumOct

        20 ->
            ConstNumHex

        21 ->
            Str

        22 ->
            StrDelim

        23 ->
            Str1

        24 ->
            Str2

        25 ->
            StrBacktick

        26 ->
            StrHereDoc

        27 ->
            StrAffix

        28 ->
            StrIntrp

        29 ->
            Chr

        30 ->
            ChrEsc

        31 ->
            Comment

        32 ->
            Comment1

        33 ->
            CommentMul

        34 ->
            CommentDoc

        35 ->
            CommentSpc

        36 ->
            Shebang

        37 ->
            Pproc

        38 ->
            PprocFile

        _ ->
            Default


name : Span -> String
name span =
    case span of
        Default ->
            "Default"

        Keyword ->
            "Keyword"

        KwdDecl ->
            "Keyword (Declaration)"

        KwdNS ->
            "Keyword (Namespace)"

        PseudoKwd ->
            "Pseudo Keyword"

        KwdReserved ->
            "Keyword (Reserved)"

        KwdType ->
            "Type"

        Name ->
            "Name"

        NameAttr ->
            "Name (Attribute)"

        NameBltn ->
            "Name (Built-in)"

        NameTag ->
            "Name (Tag)"

        Const ->
            "Constant"

        ConstBltn ->
            "Constant (Built-in)"

        ConstDate ->
            "Date Literal"

        ConstNum ->
            "Constant (Numeric)"

        ConstNumFloat ->
            "Constant (Float)"

        ConstNumInt ->
            "Constant (Int)"

        ConstNumLong ->
            "Constant (Long Int)"

        ConstNumBin ->
            "Constant (Binary)"

        ConstNumOct ->
            "Constant (Octal)"

        ConstNumHex ->
            "Constant (Hexadecimal)"

        Str ->
            "String"

        StrDelim ->
            "String Delimiter"

        Str1 ->
            "String (Single Quote)"

        Str2 ->
            "String (Double Quote)"

        StrBacktick ->
            "String (Backticks)"

        StrHereDoc ->
            "String (Here Doc)"

        StrAffix ->
            "String (Affix)"

        StrIntrp ->
            "String (Interpolation)"

        Chr ->
            "Character"

        ChrEsc ->
            "Escape Character"

        Comment ->
            "Comment"

        Comment1 ->
            "Comment (Single Line)"

        CommentMul ->
            "Comment (Multi-Line)"

        CommentDoc ->
            "Comment (Documentation)"

        CommentSpc ->
            "Comment (Special)"

        Shebang ->
            "Shebang"

        Pproc ->
            "Preprocessor"

        PprocFile ->
            "Preprocessor (File)"


nick : Span -> String
nick span =
    case span of
        Default ->
            "def"

        Keyword ->
            "kwd"

        KwdDecl ->
            "decl kwd"

        KwdNS ->
            "ns kwd"

        PseudoKwd ->
            "pseudo kwd"

        KwdReserved ->
            "rsvd kwd"

        KwdType ->
            "type"

        Name ->
            "name"

        NameAttr ->
            "attr"

        NameBltn ->
            "bltn name"

        NameTag ->
            "tag"

        Const ->
            "const"

        ConstBltn ->
            "bltn const"

        ConstDate ->
            "date"

        ConstNum ->
            "num"

        ConstNumFloat ->
            "float"

        ConstNumInt ->
            "int"

        ConstNumLong ->
            "long"

        ConstNumBin ->
            "bin"

        ConstNumOct ->
            "oct"

        ConstNumHex ->
            "hex"

        Str ->
            "str"

        StrDelim ->
            "str delim"

        Str1 ->
            "'str'"

        Str2 ->
            "\"str\""

        StrBacktick ->
            "`str`"

        StrHereDoc ->
            "here doc"

        StrAffix ->
            "affix"

        StrIntrp ->
            "str interp"

        Chr ->
            "char"

        ChrEsc ->
            "esc char"

        Comment ->
            "cmnt"

        Comment1 ->
            "1-line cmnt"

        CommentMul ->
            "multi-line cmnt"

        CommentDoc ->
            "doc cmnt"

        CommentSpc ->
            "spc cmnt"

        Shebang ->
            "#!"

        Pproc ->
            "pproc"

        PprocFile ->
            "pproc file"


all : List Span
all =
    List.range 0 38 |> List.map fromInt


fallback : Span -> Maybe Span
fallback span =
    case span of
        KwdDecl ->
            Just Keyword

        KwdNS ->
            Just Keyword

        PseudoKwd ->
            Just Keyword

        KwdReserved ->
            Just Keyword

        NameAttr ->
            Just Name

        NameBltn ->
            Just Name

        NameTag ->
            Just Name

        ConstBltn ->
            Just Const

        ConstDate ->
            Just Const

        ConstNum ->
            Just Const

        ConstNumFloat ->
            Just ConstNum

        ConstNumInt ->
            Just ConstNum

        ConstNumLong ->
            Just ConstNumInt

        ConstNumBin ->
            Just ConstNumInt

        ConstNumOct ->
            Just ConstNumInt

        ConstNumHex ->
            Just ConstNumInt

        StrDelim ->
            Just Str

        Str1 ->
            Just Str

        Str2 ->
            Just Str

        StrBacktick ->
            Just Str

        StrHereDoc ->
            Just Str

        StrAffix ->
            Just Str

        StrIntrp ->
            Just Str

        Chr ->
            Just Str

        ChrEsc ->
            Just Chr

        Comment1 ->
            Just Comment

        CommentMul ->
            Just Comment

        CommentDoc ->
            Just Comment

        CommentSpc ->
            Just Comment

        Shebang ->
            Just Comment

        PprocFile ->
            Just Pproc

        _ ->
            Nothing
