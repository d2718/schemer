port module Stylesheet exposing (..)

import Color
import Html.Attributes exposing (scope)
import Pal exposing (Palette)
import Span exposing (Span(..))
import Style exposing (Style)
import Theme exposing (Theme)


spanToClass : Span -> String
spanToClass span =
    case span of
        Default ->
            ""

        Keyword ->
            "k"

        KwdDecl ->
            "kd"

        KwdNS ->
            "kn"

        PseudoKwd ->
            "kp"

        KwdReserved ->
            "kr"

        KwdType ->
            "kt"

        Name ->
            ""

        NameAttr ->
            "na"

        NameBltn ->
            "nb"

        NameTag ->
            "nt"

        Const ->
            ""

        ConstBltn ->
            "kc"

        ConstDate ->
            "ld"

        ConstNum ->
            "m"

        ConstNumFloat ->
            "mf"

        ConstNumInt ->
            "mi"

        ConstNumLong ->
            "il"

        ConstNumBin ->
            "mb"

        ConstNumOct ->
            "mo"

        ConstNumHex ->
            "mh"

        Str ->
            "s"

        StrDelim ->
            "dl"

        Str1 ->
            "s1"

        Str2 ->
            "s2"

        StrBacktick ->
            "sb"

        StrHereDoc ->
            "sh"

        StrAffix ->
            "sa"

        StrIntrp ->
            "si"

        Chr ->
            "sc"

        ChrEsc ->
            "se"

        Comment ->
            "c"

        Comment1 ->
            "c1"

        CommentMul ->
            "cm"

        CommentDoc ->
            "sd"

        CommentSpc ->
            "cs"

        Shebang ->
            "ch"

        Pproc ->
            "cp"

        PprocFile ->
            "cpf"


getColorStr : Palette -> Maybe Int -> Maybe String
getColorStr pal n =
    n
        |> Maybe.andThen (Pal.getColor pal)
        |> Maybe.map Color.cssStr


decl : String -> Maybe String -> Maybe String
decl prop val =
    case val of
        Nothing ->
            Nothing

        Just s ->
            Just (String.join "" [ prop, ": ", s, ";" ])


getBoolStr : String -> String -> Maybe Bool -> Maybe String
getBoolStr trueVal falseVal b =
    case b of
        Nothing ->
            Nothing

        Just True ->
            Just trueVal

        Just False ->
            Just falseVal



-- font-weight: bold/normal
-- font-style: italic/normal
-- text-decoration-line: underline/none


rulesFromStyle : Theme -> Span -> Maybe String
rulesFromStyle t span =
    case span of
        Name ->
            Nothing

        Const ->
            Nothing

        _ ->
            let
                ruleSet =
                    [ Theme.getFg t span |> Maybe.map Color.cssStr |> decl "color"
                    , Theme.getBg t span |> Maybe.map Color.cssStr |> decl "background-color"
                    , Theme.getBold t span |> getBoolStr "bold" "normal" |> decl "font-weight"
                    , Theme.getItalic t span |> getBoolStr "italic" "normal" |> decl "font-style"
                    , Theme.getUline t span |> getBoolStr "underline" "none" |> decl "text-decoration-line"
                    ]
                        |> List.filterMap (\x -> x)
                        |> String.join " "
            in
            if String.length ruleSet > 0 then
                let
                    selector =
                        case span of
                            Default ->
                                "pre"

                            _ ->
                                "span." ++ spanToClass span
                in
                String.join ""
                    [ selector, " { ", ruleSet, " }" ]
                    |> Just

            else
                Nothing


sheetFromTheme : Theme -> List String
sheetFromTheme t =
    Span.all |> List.filterMap (rulesFromStyle t)



-- The hole


port toStyler : List String -> Cmd mst
