module Style exposing
    ( Style
    , copy
    , getBgCol
    , getBold
    , getFgCol
    , getItalic
    , getUline
    , new
    , noneColorVal
    , view
    )

import Color exposing (Color)
import Html exposing (Html)
import Html.Attributes as Attributes
import Pal exposing (Entry, Palette)
import Span exposing (Span)
import Util


entryCol : Entry -> Color
entryCol entry =
    entry.color


type alias Style =
    { span : Span
    , fg : Maybe Int
    , bg : Maybe Int
    , bold : Maybe Bool
    , italic : Maybe Bool
    , uline : Maybe Bool
    }



-- Constructors


new : Span -> Style
new span =
    Style span Nothing Nothing Nothing Nothing Nothing


copy : Span -> Style -> Style
copy span s =
    Style span s.fg s.bg s.bold s.italic s.uline



-- Accessors


getFgEntry : Palette -> Style -> Maybe Entry
getFgEntry pal style =
    style.fg |> Maybe.andThen (Pal.get pal)


getBgEntry : Palette -> Style -> Maybe Entry
getBgEntry pal style =
    style.bg |> Maybe.andThen (Pal.get pal)


getFgCol : Palette -> Style -> Maybe Color
getFgCol pal style =
    getFgEntry pal style |> Maybe.map entryCol


getBgCol : Palette -> Style -> Maybe Color
getBgCol pal style =
    getBgEntry pal style |> Maybe.map entryCol


getBold : Style -> Maybe Bool
getBold style =
    style.bold


getItalic : Style -> Maybe Bool
getItalic style =
    style.italic


getUline : Style -> Maybe Bool
getUline style =
    style.uline



-- Updaters


updFg : Style -> String -> Style
updFg style val =
    { style | fg = String.toInt val }


updBg : Style -> String -> Style
updBg style val =
    { style | bg = String.toInt val }


updBold : Style -> Maybe Bool -> Style
updBold style b =
    { style | bold = b }


updItalic : Style -> Maybe Bool -> Style
updItalic style b =
    { style | italic = b }


updUline : Style -> Maybe Bool -> Style
updUline style b =
    { style | uline = b }



-- View


noneColorVal : String
noneColorVal =
    "xxx"



-- colorRule : String -> Maybe Color -> Maybe (Html.Attribute a)
-- colorRule property maybeColor =
--     case maybeColor of
--         Nothing ->
--             Nothing
--         Just val ->
--             Color.cssStr val |> Attributes.style property |> Just
-- boolRule : String -> String -> String -> Maybe Bool -> Maybe (Html.Attribute a)
-- boolRule property trueVal falseVal b =
--     case b of
--         Nothing ->
--             Nothing
--         Just True ->
--             Just (Attributes.style property trueVal)
--         Just False ->
--             Just (Attributes.style property falseVal)
-- cssList : Theme -> Span -> List (Html.Attribute a)
-- cssList t span =
--     [ Theme.getFg t span |> colorRule "color"
--     , Theme.getBg t span |> colorRule "background-color"
--     , Theme.getBold t span |> boolRule "font-weight" "bold" "normal"
--     , Theme.getItalic t span |> boolRule "font-style" "italic" "normal"
--     , Theme.getUline t span |> boolRule "text-decoration-line" "underline" "none"
--     ]
--         |> Util.keepJust


colorSelectorValue : Maybe Int -> String
colorSelectorValue n =
    n |> Maybe.map String.fromInt |> Maybe.withDefault noneColorVal


view : (Style -> a) -> List (Html.Attribute a) -> List (Html a) -> Style -> Html a
view update attribs options style =
    let
        nick =
            Span.nick style.span
    in
    Html.tr
        attribs
        [ Html.td
            [ Attributes.class "span-name" ]
            [ Span.name style.span |> Html.text ]
        , Html.td []
            [ Html.select
                [ Util.onInput (updFg style >> update)
                , colorSelectorValue style.fg |> Attributes.value
                , nick ++ " foreground color" |> Attributes.title
                ]
                options
            ]
        , Html.td []
            [ Html.select
                [ Util.onInput (updBg style >> update)
                , colorSelectorValue style.bg |> Attributes.value
                , nick ++ " background color" |> Attributes.title
                ]
                options
            ]
        , Html.td [ Attributes.class "style-checks" ]
            [ Util.threeWay (updBold style >> update) (nick ++ " bold") style.bold
            , Util.threeWay (updItalic style >> update) (nick ++ " italic") style.italic
            , Util.threeWay (updUline style >> update) (nick ++ " underline") style.uline
            ]
        ]
