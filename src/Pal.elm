module Pal exposing (..)

{--
The Palette type holds, displays, and allows for the editing of
available Colors.
--}

import Color exposing (Color)
import Dict exposing (Dict)
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Util

-- Data type for holding editable text.
type Text
    = Display String
    | Editing String

-- Retrieve the text from either variant.
getText : Text -> String
getText t =
    case t of
        Display s ->
            s

        Editing s ->
            s

-- A single indexed Palette entry.
type alias Entry =
    { id : Int
    , name : Text
    , color : Color
    -- Whether the color adjustment controls should be visible.
    , expanded : Bool
    }

-- Create a new Entry with a default color.
default : Int -> Entry
default n =
    let
        name =
            "Color " ++ String.fromInt n |> Display
    in
    Entry n name Color.black False

-- Create a new Entry with the same color.
clone : Int -> Entry -> Entry
clone n ent =
    let
        name =
            "Color " ++ String.fromInt n |> Display
    in
    Entry n name (Color.copy ent.color) False

-- name getter
getName : Entry -> String
getName entry =
    getText entry.name

-- Update the given element of this Entry's color with the a hexadecimal
-- value from an <input>.
updColor : Entry -> Color.Elt -> String -> Entry
updColor ent e val =
    let
        n =
            Util.hexToByteOr 0 val
    in
    { ent | color = Color.update e ent.color n }

-- Update the given element of this Entry's color with the decimal value
-- from an <input>.
updColorDec : Entry -> Color.Elt -> String -> Entry
updColorDec ent e val =
    let
        n =
            String.toInt val |> Maybe.withDefault 0
    in
    { ent | color = Color.update e ent.color n }



--- View


minAttr : Html.Attribute a
minAttr =
    Attributes.min "0"


maxAttr : Html.Attribute a
maxAttr =
    Attributes.max "255"


nameView : (Entry -> a) -> Entry -> Html a
nameView update ent =
    case ent.name of
        Display s ->
            Html.td
                [ Attributes.class "name-entry"
                , Events.onClick ({ ent | name = Editing s } |> update)
                ]
                [ Html.text s ]

        Editing s ->
            Html.td [ Attributes.class "name-entry" ]
                [ Html.input
                    [ Util.onChange (\v -> { ent | name = Display v } |> update)
                    , Attributes.value s
                    ]
                    []
                ]


expandButton : (Entry -> a) -> Entry -> Html a
expandButton update ent =
    if ent.expanded then
        Html.td []
            [ Html.button
                [ Events.onClick ({ ent | expanded = False } |> update) ]
                [ Html.text "[^]" ]
            ]

    else
        Html.td []
            [ Html.button
                [ Events.onClick ({ ent | expanded = True } |> update) ]
                [ Html.text "[v]" ]
            ]


colorBlocks : (Entry -> a) -> Entry -> Html a
colorBlocks update ent =
    Html.div [ Attributes.class "colorblocks" ]
        [ Html.input
            [ Events.onInput (updColor ent Color.R >> update)
            , Attributes.title "red"
            , Attributes.value (Util.byteToHex ent.color.r)
            ]
            []
        , Html.input
            [ Events.onInput (updColor ent Color.G >> update)
            , Attributes.title "green"
            , Attributes.value (Util.byteToHex ent.color.g)
            ]
            []
        , Html.input
            [ Events.onInput (updColor ent Color.B >> update)
            , Attributes.title "blue"
            , Attributes.value (Util.byteToHex ent.color.b)
            ]
            []
        ]


colorBars : (Entry -> a) -> Entry -> Html a
colorBars update ent =
    Html.div [ Attributes.class "colorbars" ]
        [ Html.input
            [ Attributes.type_ "range"
            , minAttr
            , maxAttr
            , String.fromInt ent.color.r |> Attributes.value
            , Attributes.title "red"
            , Events.onInput (updColorDec ent Color.R >> update)
            ]
            []
        , Html.br [] []
        , Html.input
            [ Attributes.type_ "range"
            , minAttr
            , maxAttr
            , String.fromInt ent.color.g |> Attributes.value
            , Attributes.title "green"
            , Events.onInput (updColorDec ent Color.G >> update)
            ]
            []
        , Html.br [] []
        , Html.input
            [ Attributes.type_ "range"
            , minAttr
            , maxAttr
            , String.fromInt ent.color.b |> Attributes.value
            , Attributes.title "blue"
            , Events.onInput (updColorDec ent Color.B >> update)
            ]
            []
        ]


viewEntry : (Entry -> a) -> (Entry -> a) -> Entry -> Html a
viewEntry update drop ent =
    Html.tr
        [ Attributes.class "palette-row"
        , Color.cssStr ent.color |> Attributes.style "background-color"
        , Color.contrast ent.color |> Color.cssStr |> Attributes.style "color"
        ]
        [ nameView update ent
        , expandButton update ent
        , Html.td []
            (if ent.expanded then
                [ colorBlocks update ent, colorBars update ent ]

             else
                [ colorBlocks update ent ]
            )
        , Html.td []
            [ Html.button
                [ Attributes.class "remove-color"
                , if ent.id < 2 then
                    Attributes.disabled True

                  else
                    Events.onClick (drop ent)
                ]
                [ Html.text "[-]" ]
            ]
        ]



-- Palette Type


type alias Palette =
    { curId : Int
    , entries : Dict Int Entry
    }


empty : Palette
empty =
    Palette 0 Dict.empty


minimal : Palette
minimal =
    let
        dark =
            Entry 0 (Display "dark") Color.dark False

        light =
            Entry 1 (Display "light") Color.light False

        entries =
            Dict.singleton 0 dark |> Dict.insert 1 light
    in
    Palette 3 entries


add : Palette -> Palette
add pal =
    case Dict.values pal.entries |> List.reverse |> List.head of
        Nothing ->
            Palette
                (pal.curId + 1)
                (Dict.singleton pal.curId (default pal.curId))

        Just ent ->
            Palette
                (pal.curId + 1)
                (Dict.insert pal.curId (clone pal.curId ent) pal.entries)


remove : Palette -> Entry -> Palette
remove pal ent =
    { pal | entries = Dict.remove ent.id pal.entries }


updEnt : Palette -> Entry -> Palette
updEnt pal ent =
    { pal | entries = Dict.insert ent.id ent pal.entries }


get : Palette -> Int -> Maybe Entry
get pal n =
    Dict.get n pal.entries


getColor : Palette -> Int -> Maybe Color
getColor pal n =
    get pal n |> Maybe.map (\ent -> ent.color)



-- View


view : (Palette -> a) -> Palette -> Html a
view update pal =
    Html.div []
        [ Html.table [ Attributes.id "palette-table" ]
            [ Html.tbody []
                (List.map
                    (viewEntry (updEnt pal >> update) (remove pal >> update))
                    (Dict.values pal.entries)
                )
            ]
        , Html.div [ Attributes.id "palette-footer" ]
            [ Html.button
                [ Attributes.id "palette-add"
                , Events.onClick (add pal |> update)
                ]
                [ Html.text "[+]" ]
            ]
        ]
