module Save exposing (encodeTheme, themeDecoder)

import Color
import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Jdp
import Json.Encode as Encode
import Pal
import Span
import Style
import Theme
import Util


maybeEncode : String -> (a -> Encode.Value) -> Maybe a -> Maybe ( String, Encode.Value )
maybeEncode key encoder maybeVal =
    maybeVal |> Maybe.map (\val -> ( key, encoder val ))


decodeWithIntKeys : List ( String, a ) -> Dict Int a
decodeWithIntKeys list =
    list
        |> List.filterMap
            (\( nstr, x ) ->
                case String.toInt nstr of
                    Nothing ->
                        Nothing

                    Just n ->
                        Just ( n, x )
            )
        |> Dict.fromList


intKeyDecoder : Decoder a -> Decoder (Dict Int a)
intKeyDecoder decoder =
    Decode.map decodeWithIntKeys (Decode.keyValuePairs decoder)


textDecoder : Decoder Pal.Text
textDecoder =
    Decode.map Pal.Display Decode.string


encodeColor : Color.Color -> Encode.Value
encodeColor c =
    Encode.object
        [ ( "r", Encode.int c.r )
        , ( "g", Encode.int c.g )
        , ( "b", Encode.int c.b )
        ]


colorDecoder : Decoder Color.Color
colorDecoder =
    Decode.succeed Color.Color
        |> Jdp.required "r" Decode.int
        |> Jdp.required "g" Decode.int
        |> Jdp.required "b" Decode.int


encodeEntry : Pal.Entry -> Encode.Value
encodeEntry entry =
    Encode.object
        [ ( "id", Encode.int entry.id )
        , ( "name", Encode.string (Pal.getName entry) )
        , ( "color", encodeColor entry.color )
        ]


entryDecoder : Decoder Pal.Entry
entryDecoder =
    Decode.succeed Pal.Entry
        |> Jdp.required "id" Decode.int
        |> Jdp.required "name" textDecoder
        |> Jdp.required "color" colorDecoder
        |> Jdp.hardcoded False


encodePalette : Pal.Palette -> Encode.Value
encodePalette pal =
    Encode.object
        [ ( "cur_id", Encode.int pal.curId )
        , ( "entries", Encode.dict String.fromInt encodeEntry pal.entries )
        ]


entryFolder : List ( String, Pal.Entry ) -> Dict Int Pal.Entry
entryFolder list =
    list
        |> List.filterMap
            (\( nStr, entry ) ->
                case String.toInt nStr of
                    Nothing ->
                        Nothing

                    Just n ->
                        Just ( n, entry )
            )
        |> Dict.fromList


paletteDecoder : Decoder Pal.Palette
paletteDecoder =
    Decode.map2 Pal.Palette
        (Decode.field "cur_id" Decode.int)
        (Decode.field "entries" (intKeyDecoder entryDecoder))


encodeStyle : Style.Style -> Encode.Value
encodeStyle style =
    [ Just ( "span", Encode.int (Span.asInt style.span) )
    , maybeEncode "fg" Encode.int style.fg
    , maybeEncode "bg" Encode.int style.bg
    , maybeEncode "b" Encode.bool style.bold
    , maybeEncode "i" Encode.bool style.italic
    , maybeEncode "u" Encode.bool style.uline
    ]
        |> Util.keepJust
        |> Encode.object


styleDecoder : Decoder Style.Style
styleDecoder =
    Decode.succeed Style.Style
        |> Jdp.required "span" (Decode.map Span.fromInt Decode.int)
        |> Jdp.optional "fg" (Decode.map Just Decode.int) Nothing
        |> Jdp.optional "bg" (Decode.map Just Decode.int) Nothing
        |> Jdp.optional "b" (Decode.map Just Decode.bool) Nothing
        |> Jdp.optional "i" (Decode.map Just Decode.bool) Nothing
        |> Jdp.optional "u" (Decode.map Just Decode.bool) Nothing


encodeTheme : Theme.Theme -> Encode.Value
encodeTheme t =
    Encode.object
        [ ( "name", Encode.string t.name )
        , ( "pal", encodePalette t.pal )
        , ( "def", encodeStyle t.default )
        , ( "styles", Encode.dict String.fromInt encodeStyle t.styles )
        ]


themeDecoder : Decoder Theme.Theme
themeDecoder =
    Decode.succeed Theme.Theme
        |> Jdp.required "name" Decode.string
        |> Jdp.required "pal" paletteDecoder
        |> Jdp.required "def" styleDecoder
        |> Jdp.required "styles" (intKeyDecoder styleDecoder)
